// Make sure the global object exhists.
var global = {};

// Linked List data type.
function List() 
{
	this.start = null;
	this.end = null;
	
	this.makeNode = function() {
		return {
			data: null,
			next: null
		};
	}
	
	this.add = function(data) {
		if (this.start === null)
		{
			this.start = this.makeNode();
			this.end = this.start;
		} else {
			this.end.next = this.makeNode();
			this.end = this.end.next;
		};
	  this.end.data = data;
	 }
	 
	this.remove = function(data) {
		var current = this.start;
		var previous = this.start;
		while (current !== null)
		{
			if (data === current.data)
			{
				if (current === this.start) 
				{
					this.start = current.next;
					return;
				};
				if (current === this.end)
				{
					this.end = previous;
					previous.next = current.next;
					return;
				};
				previous = current;
				current = current.next;
			};
		};
	}
	
	this.insertAsFirst = function(d) {
		var temp = this.makeNode();
		temp.next = this.start;
		this.start = temp;
		temp.data = d;
	}
	
	this.insertAfter = function(t, d) {
		var current = this.start;
		while (current !== null)
		{
			if (current.data === t)
			{
				var temp = this.makeNode();
				temp.data = d;
				temp.next = current.next;
				if (current === this.end)
				{
					this.end = temp;
				}
				current.next = temp;
				return;
			}
			current = current.next;
		}
	}
	
	this.item = function(i) {
		var current = this.start;
		while (current !== null)
		{
			i--;
			if (i === 0)
			{
				return current;
			}
			current = current.next;
		}
		return null;
	}
	
	this.each = function(f) {
		var current = this.start;
		while (current !== null)
		{
			f(current);
			current = current.next;
		}
	}
	
	this.run = function() {
		this.each(function(what){what.data();});
	}
	
	this.close = function() {
		this.end.next = this.start;
	}
	
	this.timed = function(begin, pause) {
		begin = (typeof begin == 'undefined') ? 1 : begin;
		pause = (typeof pause == 'undefined') ? 1000 : pause;
		setTimeout(this.start.data, begin);
		var padTime = pause
		var current = this.start.next;
		while (current !== null)
		{
			setTimeout(current.data, padTime);
			current = current.next;
			padTime = padTime + pause;
		}
	}
};

// Pixel data type.
function Pixel(r,g,b,a)
{
	this.r = (typeof r == 'undefined') ? 0 : r;
	this.g = (typeof g == 'undefined') ? 0 : g;
	this.b = (typeof b == 'undefined') ? 0 : b;
	this.a = (typeof a == 'undefined') ? 1.0 : a;
	this.toHex = function () {
		return "#" + ((1 << 24) + (this.r << 16) + (this.g << 8) + this.b).toString(16).slice(1);
	}
	this.toRgbaString = function () {
		return "rgba(" + this.r + "," + this.g + "," + this.b + "," + this.a + ")";
	}
}

// Tile data type.
function Tile(imaj, type, data, passable)
{
	this.imaj = (typeof imaj == 'undefined') ? {
		art: [],
		collide: [[0,0,0,0],[0,0,0,0],[0,0,0,0],[0,0,0,0]]
	}: imaj;
	this.type = (typeof type == 'undefined') ? {
		liquid: false,
		pain: false,
		wall: false,
		hole: false,
		exit: false} : type;
	this.data = (typeof data == 'undefined') ? {
		damage: 0,
		target: [null, 0, 0]
	} : data;
	this.passable = (typeof passable == 'undefined') ? {
		right: false,
		left: false,
		up: false,
		down: false,
		above: false,
		below: false
	} : passable;
}

// Build the entire API, and do first run stuff.
global.init = function()
{
	global.container = document.getElementById("canvas");
	global.container.style.backgroundImage = "url('tile.png')";
	var tmp = "";
	tmp += "<canvas id='buffer1' width='" + global.container.style.width + "'";
	tmp += " height='" + global.container.style.height + "'>Error: No Canvas Support</canvas>";
	tmp += "<canvas id='buffer2' width='" + global.container.style.width + "'";
	tmp += " height='" + global.container.style.height + "' style='display:none;'>Error: No Canvas Support</canvas>";
	tmp += "<canvas id='buffer3' width='" + global.container.style.width + "'";
	tmp += " height='" + global.container.style.height + "' style='display:none;'>Error: No Canvas Support</canvas>";
	global.container.innerHTML = tmp;
	global.b1 = document.getElementById("buffer1");
	global.b2 = document.getElementById("buffer2");
	global.b3 = document.getElementById("buffer3");
	global.buffer1 = global.b1.getContext("2d");
	global.buffer2 = global.b2.getContext("2d");
	global.buffer3 = global.b3.getContext("2d");
	global.chain = new List();
	//
	global.data = {};
	global.data.tiles = [];
	global.data.maps = [];
	global.data.sprites = [];
	global.data.images = [];
	global.states = [];
	global.loopVar = "";
	global.gameVar = "";
	//
	if (window.location.search.length > 3)
	{
		global.getQueryVariable = function(variable)
		{
		   var query = window.location.search.substring(1);
		   var vars = query.split("&");
		   for (var i=0;i<vars.length;i++) {
			   var pair = vars[i].split("=");
			   if(pair[0] == variable){return pair[1];}
		   }
		   return(false);
		}
	} else {
		global.getQueryVariable = function(variable)
		{
			return(false);
		}
	};
	
	global.draw = (function () {
		return {
			flip: function () {
				global.buffer2.drawImage(global.b3,0,0);
			},
			flat: (function () {
				return {
					tile: function(what, where) {
						var len1 = what[0].length - 1;
						var len2 = what.length - 1;
						
						global.buffer3.lineWidth = 1.0;
						for(var a=0;a<=len1;a++)
						{
							for(var b=0;b<=len2;b++)
							{
								if (what[a][b] == "-")
								{
									//.
								} else if (what[a][b] == "?") {
									var color = "";
									color = "#";
									color += Math.floor(Math.random()*16+0).toString(16);
									color += Math.floor(Math.random()*16+0).toString(16); 
									color += Math.floor(Math.random()*16+0).toString(16);
									color += Math.floor(Math.random()*16+0).toString(16);
									color += Math.floor(Math.random()*16+0).toString(16); 
									color += Math.floor(Math.random()*16+0).toString(16);
									global.buffer3.fillStyle = color;
									global.buffer3.fillRect(where[0] + b, where[1] + a, 1, 1);
								} else {
									global.buffer3.fillStyle = what[a][b];
									global.buffer3.fillRect(where[0] + b, where[1] + a, 1, 1);
								};
							};
						};
						global.buffer3.fill();
					},
					map: function(what, where) {
						var len1 = what[0].length - 1;
						var len2 = what.length - 1;
						
						for(var a=0;a<=len1;a++)
						{
							for(var b=0;b<=len2;b++)
							{
								if (what[b][a] == "-")
								{
									//.
								} else {
									global.draw.flat.tile(global.data.tiles[what[b][a]],[16 * a + where[0], 16 * b + where[1], 0 + where[2]]);
								};
							};
						}
					}
				};
			}()),
			isometric: (function () {
				return {
					tile: function(what, where) {
						var height = what.length - 1;
						var width = what[0].length - 1;
						
						global.buffer3.lineWidth = 1;
						for(var a=0;a<=height;a++)
						{
							width = what[a].length - 1;
							for(var b=0;b<=width;b++)
							{
								if (typeof what[a][b] == "object")
								{
									var color = ""
									color += "rgba(" + what[a][b].red;
									color += ", " + what[a][b].green;
									color += ", " + what[a][b].blue;
									color += ", " + what[a][b].alpha;
									color += ")";
									global.buffer3.fillStyle = color;
									global.buffer3.fillRect(where[0] + b - a, where[1] + a + b, 2, 2);
								} else {
									if (what[a][b] == "-")
									{
										//2
									} else if (what[a][b] == "?") {
										var color = "";
										color = "#";
										color += Math.floor(Math.random()*16+0).toString(16);
										color += Math.floor(Math.random()*16+0).toString(16); 
										color += Math.floor(Math.random()*16+0).toString(16);
										color += Math.floor(Math.random()*16+0).toString(16);
										color += Math.floor(Math.random()*16+0).toString(16); 
										color += Math.floor(Math.random()*16+0).toString(16);
										global.buffer3.fillStyle = color;
										global.buffer3.fillRect(where[0] + b - a, where[1] + a + b, 2, 2);
									} else if (global.draw.blender.regEx.test(what[a][b])) {
										// Add Blender support here
										var first = what[a][b].charAt(0);
										var second = what[a][b].charAt(1);
										var color1 = "";
										var color2 = "";
										switch (first)
										{
											case "-":
												color1 = new Pixel(0,0,0,1.0);
											break;
											case "+":
												color1 = new Pixel(255,255,255,1.0);
											break;
											case "x":
												color1 = new Pixel(0,0,0,0.1);
											break;
											case "<":
												color1 = global.getPixel(where[0] + b - a - 1, where[1] + a + b - 1, "buffer");
											break;
											case "^":
												color1 = global.getPixel(where[0] + b - a + 1, where[1] + a + b - 1, "buffer");
											break;
											case ">":
												color1 = global.getPixel(where[0] + b - a + 1, where[1] + a + b + 1, "buffer");
											break;
											case "v":
											break;
										};
										switch (second)
										{
											case "-":
												color2 = new Pixel();
											break;
											case "+":
												color2 = new Pixel(255,255,255,1.0);
											break;
											case "x":
												color2 = new Pixel(color1.r,color1.g,color1.b,0.0);
											break;
											case "2":
												color2 = new Pixel(color1.r/2,color1.g/2,color1.b/2,color1.a+0.1);
											break;
											case "<":
												color2 = global.getPixel(where[0] + b - a - 1, where[1] + a + b - 1, "buffer");
											break;
											case "^":
												color2 = global.getPixel(where[0] + b - a + 1, where[1] + a + b - 1, "buffer");
											break;
											case ">":
												color2 = global.getPixel(where[0] + b - a + 1, where[1] + a + b + 1, "buffer");
											break;
											case "v":
											break;
										};
										//console.log(global.draw.blender.computeColor(color1, color2).toRgbaString());
										global.buffer3.fillStyle = global.draw.blender.computeColor(color1, color2).toRgbaString();
										global.buffer3.fillRect(where[0] + b - a, where[1] + a + b, 2, 2);
									} else {
										global.buffer3.fillStyle = what[a][b];
										global.buffer3.fillRect(where[0] + b - a, where[1] + a + b, 2, 2);
									};
								};
							};
						};
						global.buffer3.fill();
					},
					map: function(what, where) {
						var len1 = what[0].length - 1;
						var len2 = what.length - 1;

						for(var a=0;a<=len1;a++)
						{
							for(b=0;b<=len2;b++)
							{
								if (what[b][a] == "-")
								{
									//.
								} else {
									global.draw.isometric.tile(global.data.tiles[what[b][a]],[where[0] + (16 * a) - (16 * b),where[1] + (16 * b) + (16 * a),0]);
								}
							}
						}
					}
				};
			}()),
			blender: (function () {
				return {
					regEx: /[2vV<>^xX\-\+][2vV<>^xX\-\+]/,
					rgbToHex: function (r, g, b) {
						return "" + ((1 << 24) + (r << 16) + (g << 8) + b).toString(16).slice(1);
					},
					hexToRgb: function (hex) {
						// Expand shorthand form (e.g. "03F") to full form (e.g. "0033FF")
						var shorthandRegex = /^#?([a-f\d])([a-f\d])([a-f\d])$/i;
						hex = hex.replace(shorthandRegex, function(m, r, g, b) {return r + r + g + g + b + b;});

						var result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);
						return result ? {
							r: parseInt(result[1], 16),
							g: parseInt(result[2], 16),
							b: parseInt(result[3], 16),
							a: 1.0
						} : new Pixel();
					},
					computeColor: function (color1, color2) {
						var c1 = (typeof color1 == 'Pixel') ? this.hexToRgb(color1) : color1;
						var c2 = (typeof color2 == 'Pixel') ? this.hexToRgb(color2) : color2;
						
						var c3r1 = Math.round(c1.r + c2.r / 2);
						var c3g1 = Math.round(c1.g + c2.g / 2);
						var c3b1 = Math.round(c1.b + c2.b / 2);
						var c3a1 = c1.a;
						
						var c3r2 = Math.round(c2.r + c1.r / 2);
						var c3g2 = Math.round(c2.g + c1.g / 2);
						var c3b2 = Math.round(c2.b + c1.b / 2);
						var c3a2 = c2.a;
						
						var c3r = c3r1 < c3r2 ? c3r1 : c3r2;
						var c3g = c3g1 < c3g2 ? c3g1 : c3g2;
						var c3b = c3b1 < c3b2 ? c3b1 : c3b2;
						var c3a = c1.a - c2.a / (1 + c1.a)//c3a1 > c3a2 ? c3a1 : c3a2;
						
						c3r = c3r >= 255 ? 255 : c3r;
						c3g = c3g >= 255 ? 255 : c3g;
						c3b = c3b >= 255 ? 255 : c3b;
						c3a = c3a >= 1.0 ? 1.0 : c3a;
						c3a = c3a <= 0.0 ? 0.0 : c3a;
						
						//console.log(c1.a, c2.a, c3a);
						
						var c3 = new Pixel(c3r, c3g, c3b, c3a);//this.rgbToHex(c3r, c3g, c3b);

						return c3;
					},
					computeColors: function (color1, color2, length) {
						
					}
				};
			}())
		};
	}());
	
	global.create = (function () {
		return {
			solid: function(color, width, height) {
				var tmp = [];
				if (typeof color == "object")
						{
							var tmpc = ""
							tmpc += "rgba(" + color.red;
							tmpc += ", " + color.green;
							tmpc += ", " + color.blue;
							tmpc += ", " + color.alpha;
							tmpc += ")";
							color = tmpc;
				} else {
					if(color == "?")
					{
						color = "#";
						color += Math.floor(Math.random()*16+0).toString(16);
						color += Math.floor(Math.random()*16+0).toString(16); 
						color += Math.floor(Math.random()*16+0).toString(16);
						color += Math.floor(Math.random()*16+0).toString(16);
						color += Math.floor(Math.random()*16+0).toString(16); 
						color += Math.floor(Math.random()*16+0).toString(16);
					}
				}
				for(var z=0;z<=height - 1;z++)
				{
					tmp.push([]);
				}
				for(var a=0;a<=height - 1;a++)
				{
					for(var b=0;b<=width - 1;b++)
					{
						tmp[a].push(color);
					}
				}
				return tmp;
			},
			fromTemplate: function(template) {
				
			}
			// create.random.solid()
			// create.random.blend()
		};
	}());
	
	global.getPixel = function(x, y, which) {
		if (which == "canvas")
		{
			var tmp = global.buffer1.getImageData(x,y,1,1);
			tmp.data[3] = tmp.data[3] >= 1.0 ? tmp.data[3] / 2.55 / 100 : tmp.data[3];
			return new Pixel(tmp.data[0],tmp.data[1],tmp.data[2],tmp.data[3]);
		} else {
			var tmp = global.buffer3.getImageData(x,y,1,1);
			tmp.data[3] = tmp.data[3] >= 1.0 ? tmp.data[3] / 2.55 / 100 : tmp.data[3];
			return new Pixel(tmp.data[0],tmp.data[1],tmp.data[2],tmp.data[3]);
		};
	};
	
	global.input = (function () {
		return {
			keyboard: function (e) {
				e.preventDefault();
				//var key = {};
				//key.code = (e.keyCode == 0) ? e.charCode : e.keyCode;
				//key.char = String.fromCharCode(key.code).toLowerCase();
				global.interaction(e);
			},
			mouse: function (e) {
				e.preventDefault();
				global.interaction(e);
			},
			touch: function (e) {
				e.preventDefault();
				global.interaction(e);
			}
		};
	}());
	
	global.getResolution = function() {
		global.width = (screen.availWidth < window.innerWidth) ? screen.availWidth : (document.body.clientWidth < window.innerWidth) ? document.body.clientWidth : window.innerWidth;
		global.height = (screen.availHeight < window.innerHeight) ? screen.availHeight : (document.body.clientHeight < window.innerHeight) ? document.body.clientHeight : window.innerHeight
		//window.innerHeight: 683
		//document.body.clientHeight: 683
	};
	
	global.setResolution = function() {
		global.container.style.width = global.width;
		global.container.style.height = global.height;
	};
	
	global.resized = function() {
		//global.getResolution();
		//global.setResolution();
		
		global.container = document.getElementById("canvas");
		global.container.style.backgroundImage = "url('tile.png')";
		var tmp = "";
		tmp += "<canvas id='buffer1' width='" + global.container.style.width + "'";
		tmp += " height='" + global.container.style.height + "'>Error: No Canvas Support</canvas>";
		tmp += "<canvas id='buffer2' width='" + global.container.style.width + "'";
		tmp += " height='" + global.container.style.height + "' style='display:none;'>Error: No Canvas Support</canvas>";
		tmp += "<canvas id='buffer3' width='" + global.container.style.width + "'";
		tmp += " height='" + global.container.style.height + "' style='display:none;'>Error: No Canvas Support</canvas>";
		global.container.innerHTML = tmp;
		global.b1 = document.getElementById("buffer1");
		global.b2 = document.getElementById("buffer2");
		global.b3 = document.getElementById("buffer3");
		global.buffer1 = global.b1.getContext("2d");
		global.buffer2 = global.b2.getContext("2d");
		global.buffer3 = global.b3.getContext("2d");
		global.redraw();
	};
	
	global.startLoop = function() {
		global.loopVar = setInterval(global.flush,50);
		global.gameVar = setInterval(global.gameLoop,50);
	};

	global.flush = function() {
		//
		var tickTime = new Date()
		document.title = tickTime.getTime();
		global.buffer1.drawImage(global.b2,0,0);
	};

	//global.getResolution();
	//global.setResolution();
	document.body.addEventListener("keydown", global.input.keyboard, true);
	document.body.addEventListener("keyup", global.input.keyboard, true);
	document.body.addEventListener("keypress", global.input.keyboard, true);
	//
	document.body.addEventListener("mousedown", global.input.mouse, true);
	document.body.addEventListener("mouseup", global.input.mouse, true);
	document.body.addEventListener("mousemove", global.input.mouse, true);
	//
	document.body.addEventListener("touchstart", global.input.touch, true);
	document.body.addEventListener("touchend", global.input.touch, true);
	document.body.addEventListener("touchcancel", global.input.touch, true);
	document.body.addEventListener("touchleave", global.input.touch, true);
	document.body.addEventListener("touchmove", global.input.touch, true);

	window.addEventListener("resize", global.resized, true);
	//
	////
	global.loadData();
	global.main();
}